//
//  AppDelegate.h
//  tableview
//
//  Created by Prince Prabhakar on 05/10/15.
//  Copyright (c) 2015 Prince Prabhakar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

